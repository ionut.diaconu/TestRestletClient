﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace TestRestletClient
{
    [DataContract]
    class MyObjectList
    {
        [DataMember]
        public List<MyObject> myObjectList { get; set; }
    }
}
