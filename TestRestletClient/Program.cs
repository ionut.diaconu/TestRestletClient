﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCCDashboard;

namespace TestRestletClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            

        }


        public static async Task testRestCall()
        {
            Console.WriteLine("getting rest response");

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("user-agent", "tcc-agent");

                    HttpResponseMessage response = await httpClient.GetAsync("http://localhost:8182/test/myres");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(responseBody)))
                    {
                        // Deserialization from JSON  
                        DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(MyObjectList));
                        MyObjectList bsObj2 = (MyObjectList)deserializer.ReadObject(ms);

                        foreach (MyObject ob in bsObj2.myObjectList)
                        {
                            Console.WriteLine("name: " + ob.name);
                            Console.WriteLine("age: " + ob.age);
                        }

                    }


                    Console.WriteLine(responseBody);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
